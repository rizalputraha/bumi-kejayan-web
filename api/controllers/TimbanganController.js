/**
 * TimbanganController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/library/LibraryController');

const TimbanganController = {
    create : async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            // const {id_user} = res.locals.udata.payload;
            // req.body.id_user = id_user;
            const result = await TimbanganController.createProcess(req.body);
            return result;
        });
    },

    createProcess : async (body) => {
        const query = await Timbangan.create(body).fetch().meta({skipRecordVerification: true});
        return query;
    },

    read : async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {filter} = req.query;
            let query;
            if (filter == 'all' || !filter) {
                query = await Timbangan.find({}).sort('created_at ASC');
            } else if(filter == 'open') {
                query = await Timbangan.find({where: {status: 1}}).sort('created_at ASC');
            } else if(filter == 'termuat') {
                query = await Timbangan.find({where: {status: 2}}).sort('created_at ASC');
            } else {
                throw new Error('Data Not Found!');
            }
            const result = LibraryController.addNomerList(query);
            return result;
        });
    },

    readPaging : async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {filter} = req.query;
            const {page, perPage} = req.params;
            let query;
            let total;
            if (filter == 'all' || !filter) {
                query = await Timbangan.find({limit: perPage, skip: page * perPage - perPage}).sort('created_at ASC');
                total = await Timbangan.count();
            } else if(filter == 'open') {
                query = await Timbangan.find({where: {status: 1}, limit: perPage, skip: page * perPage - perPage}).sort('created_at ASC');
                total = await Timbangan.count({status: 1});
            } else if(filter == 'termuat') {
                query = await Timbangan.find({where: {status: 2}, limit: perPage, skip: page * perPage - perPage}).sort('created_at ASC');
                total = await Timbangan.count({status: 2});
            } else {
                throw new Error('Data Not Found!');
            }
            const result = LibraryController.addNomerList(query);
            return {data:result, total};
        });
    },

    edit : async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {id} = req.params;
            const result = await TimbanganController.updateProcess(id, body);
            return result;
        });
    },

    updateProcess : async (id, body) => {
        const query = await Timbangan.updateOne({id: id}).set(body).meta({skipRecordVerification: true});;
        return 'Sukses update timbangan';
    }
};

module.exports = TimbanganController;

