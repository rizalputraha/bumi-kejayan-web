/**
 * AuthLibraryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

let env = require('../../../env');
var crypto = require('crypto');

const AuthLibraryController = {
    createSalt: (email, pass) => {
        let hash = crypto.createHmac('md5', env.SALT);
        hash.update(pass);
        hash.update(email.email.toLowerCase());

        let hashed = hash.digest('hex');
        return hashed;
    },
}

module.exports = AuthLibraryController;

