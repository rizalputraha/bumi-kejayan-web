/**
 * SignupController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../library/LibraryController');
const AuthLibraryController = require('../library/AuthLibraryController');

const SignupController = {
  register: async function (req, res) {
    await LibraryController.response(req, res, async (body) => {
      let resp = await SignupController.signupProcess(req.body);
      return resp;
    })
  },

  signupProcess: async function (body) {
    const findEmail = await User.findOne({ email: body.email });
      if (findEmail) {
        throw new Error('Email Sudah Terdaftar');
      } else {
        let hashed = AuthLibraryController.createSalt({ email: body.email }, body.password);
        let obj = { ...body, password: hashed };
        let res = await User.create(obj).fetch();
        return "Sukses membuat data user";
      }
  }
}

module.exports = SignupController;

