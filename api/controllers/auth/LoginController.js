/* eslint-disable indent */
/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var jwt = require('jsonwebtoken');
const SESSION = [];
let env = require('../../../env');
const LibraryController = require('../library/LibraryController');
const AuthLibraryController = require('../library/AuthLibraryController');

const LoginController = {
    deleteSession: function (id) {
        const udata = SESSION[`user_${id}`];
        clearTimeout(udata.timeout);
        delete SESSION[`user_${id}`];
        console.log('deleteSession', SESSION);
    },

    sessionExist: function (userdata, crc) {
        const idx = `user_${userdata.id}`;
        const udata = SESSION[idx];

        if (udata) {
            const menit = idleTime / 6000;
            console.log(udata.crc == crc);

            return (udata.crc == crc) ? true : { error: 401, message: `Anda sedang login di device lain, mohon logout!` };
        }
        return { error: 401,
            data: [],
            message: 'Token Invalid! or Session Expired',
            stack: {},
            errorName: '', 
            noExist: true};
    },

    createToken: function (user) {
        let signerOption = {
            issuer: 'mfi',
            expiresIn: '1d',
        }
        let payload = {
            id_user: user.id,
            name: user.nama,
            role: user.role,
            email: user.email,
            status: user.status
        }
        const tokenAction = jwt.sign(payload, env.privatKey, signerOption);
        return tokenAction;
    },

    checkAuthUser: async function (email) {
        let user = await User.findOne({ email: email });
        return user;
    },

    login: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let checkUser = await LoginController.checkAuthUser(req.body.email);
            if (checkUser) {
                if (checkUser.status == 1) {
                    throw new Error('User dibanned untuk sementara waktu, untuk pengecekan harap hubungi Superadmin');
                } else if (checkUser.status == 2) {
                    throw new Error('User sudah tidak aktif, untuk pengecekan harap hubungi Superadmin');
                } else {
                    let hashedPw = AuthLibraryController.createSalt({ email: checkUser.email }, req.body.password);
                    if (hashedPw == checkUser.password) {
                        const token = LoginController.createToken(checkUser);
                        return token;
                    } else {
                        throw new Error('Password salah!')
                    }
                }
            }
            throw new Error('User not Found!');
        })
    },

    logout: async function (udata, token) {
        const exists = LoginController.sessionExist(udata, token);
        if (exists === true) {
            const oke = await LoginController.deleteSession(udata.id)
            return true
        }
        return exists;
    }
}

module.exports = LoginController;
