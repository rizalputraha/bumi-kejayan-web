/**
 * Timbangan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "t_timbangan",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_timbangan',
      autoIncrement: true
    },

    id_user: {
      model: 'user'
    },

    id_pelanggan: {
      model: 'pelanggan'
    },

    id_produk: {
      model: 'produk'
    },
    
    kd_timbangan: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    nopol: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    material: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    sopir: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    masuk: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    keluar: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    bruto: {
      type: 'number',
    },

    tara: {
      type: 'number',
    },

    netto: {
      type: 'number',
    },

    status: {
      type: 'number',
    },

    created_at: {
      type: 'string',
      autoCreatedAt: true,
    },
    
    updated_at: {
      type: 'string',
      autoUpdatedAt: true,
    },
  },
};

