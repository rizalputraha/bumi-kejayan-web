/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "t_user",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_user',
      autoIncrement: true
    },
    
    nama: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    role: {
      type: 'number',
      required: true,
    },

    email: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 100
    },

    password: {
      type: 'string',
      required: true,
      protect: true
    },

    status: {
      type: 'number',
    },

    created_at: {
      type: 'string',
      autoCreatedAt: true,
    },
    
    updated_at: {
      type: 'string',
      autoUpdatedAt: true,
    },
  },
};

